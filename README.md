# Stripe payment method

## Introduction

This is a React app that uses Stripe Elements to generate a payment method token. This example code is useful for when you want to use Stripe billing to generate recurring subscriptions and charge the credit card periodically. In this case you will need a payment method. The steps are:

1. Use this web form to capture the card details
2. Inspect the console to retrieve the payment method
3. Attach the payment method to the customer using the Stripe API
4. Update the customer record to set the payment method as the default payment method

## Install instructions

```
git clone git@gitlab.com:raytio-public/tools/stripe-payment-method.git
cd stripe-payment-method
yarn install
```

## Configure instructions

Open index.js and update `apiKey` with your test Stripe API key

## Run

```
yarn start
```
