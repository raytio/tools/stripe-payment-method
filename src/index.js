/* @license
Copyright Raytio Ltd. All Rights Reserved.

Use of this source code is governed by an MIT-style license that can be
found in the LICENSE file at https://www.rayt.io/mit */

import React from "react";
import ReactDOM from "react-dom";
import {
  Elements,
  CardElement,
  StripeProvider,
  injectStripe
} from "react-stripe-elements";

import "./styles.css";

const StripePage = injectStripe(({ stripe }) => {
  const { createPaymentMethod } = stripe;

  function handleSubmit(e) {
    console.log("submitting");
    e.preventDefault();
    createPaymentMethod("card", CardElement).then(({ paymentMethod }) => {
      console.log(paymentMethod);
    });
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <CardElement />
        <button htmltype="submit">Submit</button>
      </form>
    </div>
  );
});

function App() {
  return (
    <div className="App">
      <StripeProvider apiKey="pk_test_1234567890">
        <Elements>
          <StripePage />
        </Elements>
      </StripeProvider>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
